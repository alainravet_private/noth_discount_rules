# Guide : how to use this library code and the discounting rules


# step 1 :
#   load the production code and the discounting rules :
#------------------------------------------------------
require_relative 'lib/_load_all'


# step 2 :
#   Load the shop items details in the db/repository :
#---------------------------------------------


REPO = ItemsRepository.new.tap do |repo|
  repo << Item.new(1, 'Travel Card Holder',      9.25 * 100)
  repo << Item.new(2, 'Personalised cufflinks', 45.00 * 100)
  repo << Item.new(3, 'Kids T-shirt',           19.95 * 100)
end


# step 3 :
#   pass the discounting rules to checkout :
#------------------------------------------

active_discount_rules = [ Rules::DISCOUNT_IF_MULTIPLE_TRAVEL_CARD_HOLDERS,  # price -> £8.50
                          Rules::DISCOUNT_ABOVE_60_GBP,                     # discount : 10%
                        ]
co = Checkout.new(active_discount_rules)
co.scan REPO, [1, 2, 3     ]
puts co.total #=> 66.78


# step 4 (optional) :
#   How to define a new discounting rule?
# Example below : never pay more than £33
#------------------------------------------

max_order_total_is_33_gbp = Rule.new.tap do |rule|

  def rule.apply_discount_to(order)
    max_price_in_cents = 33 * 100

    return if order.subtotal_in_cents <= max_price_in_cents

    discount  = order.subtotal_in_cents - max_price_in_cents
    order.subtotal_discount_in_cents = discount
  end

end


active_discount_rules = [ max_order_total_is_33_gbp]

co = Checkout.new(active_discount_rules)

co.scan REPO, [1]       ; puts co.total   #=> 9.25
co.scan REPO, [3]       ; puts co.total   #=> 29.2
co.scan REPO, [1, 2, 3 ]; puts co.total   #=> 33.0
