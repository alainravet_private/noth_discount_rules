# This is the template for discounting rules

class Rule

  def apply_discount_to(order)
    raise "apply_discount_to(order) method not implemented in #{self.class}"
  end

end
