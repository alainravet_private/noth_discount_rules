class Item < Struct.new(:id, :description, :price_in_cents)

  def price_in_cents
    super.to_i
  end

  def to_s
    "Item: #{id}, #{description}, #{price_in_cents} cents"
  end

  def hash
    id
  end
end
