# Rule : If   you buy 2 or more travel card holders
#        Then the unit rice drops to £8.50

module Rules

  DISCOUNT_IF_MULTIPLE_TRAVEL_CARD_HOLDERS = Rule.new.tap do |rule|
    def rule.apply_discount_to(order)
      order.lines.each do |line|
        next unless discounted?(line)

        discounted_price_in_cents = 850
        discount = line.item.price_in_cents - discounted_price_in_cents

        line.unit_discount_in_cents = discount
      end
    end

    def rule.discounted?(line)
      (line.quantity >= 2) && (line.item.description == 'Travel Card Holder')
    end
  end

end
