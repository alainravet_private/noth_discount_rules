# Rule : If   you spend over £60,
#        Then you get 10% off your purchase

module Rules

  DISCOUNT_ABOVE_60_GBP = Rule.new.tap do |rule|

    def rule.apply_discount_to(order)
      return unless discounted?(order)

      discount = order.subtotal_in_cents * 0.1

      order.subtotal_discount_in_cents = discount
    end

    def rule.discounted?(order)
      minimum_amount = 60 * 100
      order.subtotal_in_cents > minimum_amount
    end
  end

end
