# the ItemRepository contains all the Items a visitor can order

class ItemsRepository

  def initialize
    @contents = {}
  end

  def <<(item)
    @contents[item.id] = item
    self
  end

  def find(id)
    @contents[id]
  end

end
