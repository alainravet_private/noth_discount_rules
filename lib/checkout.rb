class Checkout

  def initialize(discount_rules=[])
    @basket             = Basket.new
    @discount_rules     = Array(discount_rules)
  end

  def scan(repository, item_or_items_ids)
    Array(item_or_items_ids).each do |item_id|
      item = repository.find(item_id)
      @basket << item
    end
  end

  # Apply the discounts rules on all the scanned items and return the total price in GBP
  def total
    (compute_total_in_cents / 100.0).round(2)
  end

private

  def compute_total_in_cents
    order = Order.new(@basket)

    @discount_rules.each do |rule|
      rule.apply_discount_to order
    end

    order.total_in_cents
  end

end
