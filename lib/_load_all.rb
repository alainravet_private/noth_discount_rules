# load all the production files and all the defined discount rules

require_relative 'item'
require_relative 'items_repository'
require_relative 'basket'
require_relative 'order'
require_relative 'rule'
require_relative 'rules'
require_relative 'checkout'
