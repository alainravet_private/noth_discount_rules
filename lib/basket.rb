# a Basket simply contains all the items ordered by a visitor.

class Basket

  attr_reader :items

  def initialize(items=[])
    @items = items
  end

  def <<(item)
    @items << item
    self
  end

  def count(item)
    items.count(item)
  end
end
