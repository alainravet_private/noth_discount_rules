class Order

  class Line < Struct.new(:item, :quantity)

    def initialize(item, quantity)
      super
      @unit_discount_in_cents       = 0
    end
    attr_accessor :unit_discount_in_cents

    def gross_unit_price_in_cents
      item.price_in_cents
    end

    def net_unit_price_in_cents
      gross_unit_price_in_cents - unit_discount_in_cents
    end

    def total_in_cents
      net_unit_price_in_cents * quantity
    end
  end
end
