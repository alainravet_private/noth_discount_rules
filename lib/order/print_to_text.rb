require 'erb'

class Order
  module PrintToText
    def to_text
      erb = ERB.new(File.read(order_template_filename))

      order = self  # .. so `order`is in the bindings
      erb.result(binding)
    end

  private

    def order_template_filename
      File.expand_path File.dirname(__FILE__) + '/templates/order_model.text.erb'
    end

  end
end
