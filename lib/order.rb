# An Order is a sequence of Lines that list the ordered items, quantities, and the discounts.

require_relative 'order/line'
require_relative 'order/print_to_text'

class Order

  include PrintToText

  def initialize(basket=nil)
    @subtotal_discount_in_cents  = 0
    convert_basket_to_lines(basket)
  end

  attr_reader :lines
  attr_accessor :subtotal_discount_in_cents

  def subtotal_in_cents
    lines.map(&:total_in_cents).reduce(&:+) || 0
  end


  def total_in_cents
    subtotal_in_cents - @subtotal_discount_in_cents
  end

  def add_line(line)
    @lines << line
    self
  end


private

  def convert_basket_to_lines(basket)
    @lines              = []
    return unless basket
    basket.items.group_by{|i| i}.map{|k,v| [k, v.count] }.each do |item, quantity|
      add_line Line.new(item, quantity)
    end
  end

end
