#load the production code :
require_relative '../lib/_load_all'

def assert_cart_total(rules, basket, expected)
  co = Checkout.new(rules)
  co.scan REPO, basket
  return if (success = co.total == expected)
  raise "Invalid total : got £#{co.total}, expected £#{expected}" unless success
end

# uncomment for Rubymine:
#   require 'minitest/reporters'
#   MiniTest::Reporters.use!

require 'minitest/autorun'
