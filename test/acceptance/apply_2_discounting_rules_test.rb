# Our marketing team wants to offer promotions as an incentive for
# our customers to purchase these items.
# * If you spend over £60, then you get 10% off your purchase
# * If you buy 2 or more travel card holders then the unit price drops to £8.50.

require_relative '../test_helper'

# Load the shop items in the db/repository :
#--------------------------------------------

REPO = ItemsRepository.new.tap do |repo|
  repo << Item.new(1, 'Travel Card Holder',      9.25 * 100)
  repo << Item.new(2, 'Personalised cufflinks', 45.00 * 100)
  repo << Item.new(3, 'Kids T-shirt',           19.95 * 100)
end


# Select which discounting rules to apply, and in which order :
#--------------------------------------------------------------

ACTIVE_DISCOUNT_RULES = [Rules::DISCOUNT_IF_MULTIPLE_TRAVEL_CARD_HOLDERS, Rules::DISCOUNT_ABOVE_60_GBP]


#########
# test : (expected values copied from the requirements document)
#########

assert_cart_total(ACTIVE_DISCOUNT_RULES, [001, 002, 003     ], 66.78)
assert_cart_total(ACTIVE_DISCOUNT_RULES, [001, 003, 001     ], 36.95)
assert_cart_total(ACTIVE_DISCOUNT_RULES, [001, 002, 001, 003], 73.76)

puts "Success"


__END__
warning : 003 is an octal number
