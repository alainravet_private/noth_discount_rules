require_relative '../test_helper'

class ReadyToPrintOrderTest < MiniTest::Unit::TestCase

  EXPECTED_OUTPUT = <<TEXT
ORDER SUMMARY :

    ITEM                         QUANTITY       UNIT       UNIT         SUBTOTAL
                                              PRICE (£)  DISCOUNT (£)
------------------------------------------------------------------------------------
    Travel Card Holder              2            9.25 £     -0.75 £       17.00 £
    Personalised cufflinks          1           45.00 £     -0.00 £       45.00 £

------------------------------------------------------------------------------------
subtotal :                                                                62.00 £
discount :                                                                -6.20 £
------------------------------------------------------------------------------------
TOTAL    :                                                                55.80 £
------------------------------------------------------------------------------------
TEXT


  def test_to_text
    @order = prepare_order
    @order.to_text.must_equal EXPECTED_OUTPUT
  end

private

  def prepare_order
    item_1 = Item.new(1, 'Travel Card Holder', 9.25 * 100)
    item_2 = Item.new(2, 'Personalised cufflinks', 45.00 * 100)

    order = Order.new(Basket.new <<
                           item_1 <<
                           item_2 <<
                           item_1)

    [Rules::DISCOUNT_IF_MULTIPLE_TRAVEL_CARD_HOLDERS, # price : 9.25 -> £8.50
     Rules::DISCOUNT_ABOVE_60_GBP, # global discount : 10%
    ].each do |rule|
      rule.apply_discount_to order
    end
    order
  end

end
