# an ItemRepository is an Item container with
#
require_relative '../../test_helper'

class DiscountIfTwoOrMoretravelCardHolderTest < MiniTest::Unit::TestCase

  def rule
    Rules::DISCOUNT_IF_MULTIPLE_TRAVEL_CARD_HOLDERS
  end

  def setup
    @travel_card = Item.new(1, 'Travel Card Holder',      9.25 * 100)
    @cufflings   = Item.new(2, 'Personalised cufflinks', 45.00 * 100)
  end


  def test_no_discount_if_condition_not_met
    order   = Order.new Basket.new([@travel_card, @cufflings])
    rule.apply_discount_to(order)

    order.total_in_cents    .must_equal (925 * 1) + (4500 * 1)

    order.lines[0].unit_discount_in_cents.must_equal 0
    order.lines[1].unit_discount_in_cents.must_equal 0
  end

  def test_discount_is_applied_to_order_if_condition_is_met
    basket  = Basket.new([@travel_card, @travel_card,  # order twice ==> discount
                           @cufflings])
    order   = Order.new(basket)
    rule.apply_discount_to(order)

    order.total_in_cents    .must_equal (850 * 2) + (4500 * 1)

    order.lines[0].unit_discount_in_cents.must_equal (925 - 850)
    order.lines[1].unit_discount_in_cents.must_equal 0
  end

end
