# an ItemRepository is an Item container with 
#
require_relative '../test_helper'


class OrderLineTest < MiniTest::Unit::TestCase

  def setup
    @item_1 = Item.new(1, 'Travel Card Holder',      9.25 * 100)
    @item_2 = Item.new(2, 'Personalised cufflinks', 45.00 * 100)
  end

  def test_order_line_for_item_bought_twice
    Order::Line.new(@item_1, quantity=2).tap do |ol|
      # virgin line (with NO discount)
      ol.item                      .must_equal @item_1
      ol.quantity                  .must_equal 2
      ol.gross_unit_price_in_cents .must_equal 925
      ol.net_unit_price_in_cents   .must_equal 925   # default value : == gross price
      ol.unit_discount_in_cents    .must_equal 0     # default value : no discount
      ol.total_in_cents            .must_equal 925 * 2

    end.tap do |ol|
      # add a 10% discount :
      ol.unit_discount_in_cents = 92

      ol.net_unit_price_in_cents   .must_equal (925 -92)
      ol.total_in_cents            .must_equal (925 -92)* 2
    end
  end

end


class OrderTest < MiniTest::Unit::TestCase

  def test_empty_order
    Order.new.tap do |order|
      order.total_in_cents    .must_equal 0
      order.lines.must_equal []
    end
  end

end


class CreateOrderFromBasketTest  < MiniTest::Unit::TestCase

  def setup
    @item_1 = Item.new(1, 'Travel Card Holder',      9.25 * 100)
    @item_2 = Item.new(2, 'Personalised cufflinks', 45.00 * 100)
    @basket = Basket.new
  end

  def test_create_order_from_basket
    @basket << @item_1 << @item_2 << @item_1 << @item_1 << @item_2
    order   = Order.new(@basket)
    order.lines[0].must_equal Order::Line.new(@item_1, quantity=3)
    order.lines[1].must_equal Order::Line.new(@item_2, quantity=2)

    order.total_in_cents    .must_equal (925 * 3) + (4500 * 2)
  end

end
