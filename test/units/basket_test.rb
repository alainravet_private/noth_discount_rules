# a Basket is a simple Item container
#
require_relative '../test_helper'

class BasketTest < MiniTest::Unit::TestCase

  def setup
    @basket = Basket.new
    @item_1 = @travel_card = Item.new(1, 'Travel Card Holder',      9.25 * 100)
    @item_2 = @cufflinks   = Item.new(2, 'Personalised cufflinks', 45.00 * 100)
  end

  def test_place_and_retrieve_items_in_the_basket
    @basket << @item_1 << @item_2 << @item_1
    @basket.items.must_equal [@item_1, @item_2, @item_1]
  end

  def test_load_basket_during_initialisation
    basket = Basket.new([@item_1, @item_2, @item_1])
    basket.items.must_equal [@item_1, @item_2, @item_1]
  end

  def test_count
    @basket << @travel_card << @cufflinks
    @basket.count(@travel_card).must_equal 1
    @basket << @travel_card
    @basket.count(@travel_card).must_equal 2
  end

end
