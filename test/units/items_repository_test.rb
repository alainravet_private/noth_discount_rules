# an ItemRepository is an Item container with 
#
require_relative '../test_helper'

class ItemsRepositoryTest < MiniTest::Unit::TestCase

  def setup
    @repository = ItemsRepository.new  # empty
    @item_1 = Item.new(1, 'Travel Card Holder',      9.25 * 100)
    @item_2 = Item.new(2, 'Personalised cufflinks', 45.00 * 100)
  end

  def test_items_can_be_stored_and_retrieved
    # add with `<<`
    @repository << @item_1 << @item_2 << @item_1
    # retrieve with `find`
    @repository.find(@item_1.id).must_equal @item_1
    @repository.find(@item_2.id).must_equal @item_2
  end

  def test_find_returns_nil_when_no_item_matches_the_id
    unknown_id = 666
    @repository.find(unknown_id).must_equal nil
  end

end
