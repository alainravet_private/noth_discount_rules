tests result : [ ![Codeship Status for alainravet/noth_discount_rules](https://www.codeship.io/projects/87a8c360-09a6-0132-caf8-660941b3e595/status)](https://www.codeship.io/projects/31566)

Changelog: 
===============
* NEW : ```Order#to_text``` converts an order into a ready-to-print text  
  (see ```test/acceptance/display_ready_to_print_order_test.rb```)
  
Example :    

    ORDER SUMMARY :
    
    ITEM                         QUANTITY       UNIT       UNIT         SUBTOTAL
                                              PRICE (£)  DISCOUNT (£)
    ------------------------------------------------------------------------------------
    Travel Card Holder              2            9.25 £     -0.75 £       17.00 £
    Personalised cufflinks          1           45.00 £     -0.00 £       45.00 £
    ------------------------------------------------------------------------------------
    subtotal :                                                            62.00 £
    discount :                                                            -6.20 £
    ------------------------------------------------------------------------------------
    TOTAL    :                                                            55.80 £
    ------------------------------------------------------------------------------------



  
Objective:
===============
    Add support for discounting rules that can be activated and combined at checkout time
    to compute an order total based on business rules.

    # Our marketing team wants to offer promotions as an incentive for
    # our customers to purchase these items.
    # * If you spend over £60, then you get 10% off your purchase
    # * If you buy 2 or more travel card holders then the unit price drops to £8.50.
    

Code reading guide :
=====================
* usage : see ```how_to_use_this_library_and_rules.rb```
* the discounting ```rules are stored in lib/rules/*.rb```)

Domain vocabulary :
-------------------
* an ```Item``` is an object that can be ordered online (price + description).
* the ```Basket``` collects all the ```Items``` ordered by a visitor.
* the ```ItemRepository``` contains all the known ```Items```.
* at ```Checkout```-time, an ```Order``` is built (internally) that lists all the ```Items```, their prices, quantities and the various discounts.
* an ```Order``` comprises zero or more ```OrderLines```
* After all the discount```Rules``` have been applied in sequence to an ```Order```, its ```total_in_cents``` and ```total``` are the final result.



requirements :
---------------------------------------------------------------
Ruby 2.*  
bundler


How to run the tests :
========================
    $ git clone git@bitbucket.org:alainravet/noth_discount_rules.git
    $ cd noth_discount_rules
    $ bundle
    $ bundle exec rake



How to use this library :
========================

    # step 1 :
    #   load the production code and the discounting rules :
    #------------------------------------------------------
    require_relative 'lib/_load_all'
    
    
    # step 2 :
    #   Load the shop items details in the db/repository :
    #---------------------------------------------
    
    
    REPO = ItemsRepository.new.tap do |repo|
      repo << Item.new(1, 'Travel Card Holder',      9.25 * 100)
      repo << Item.new(2, 'Personalised cufflinks', 45.00 * 100)
      repo << Item.new(3, 'Kids T-shirt',           19.95 * 100)
    end
    
    
    # step 3 :
    #   pass the discounting rules to checkout :
    #------------------------------------------
    
    active_discount_rules = [ Rules::DISCOUNT_IF_MULTIPLE_TRAVEL_CARD_HOLDERS,  # price -> £8.50
                              Rules::DISCOUNT_ABOVE_60_GBP,                     # discount : 10%
                            ]
    co = Checkout.new(active_discount_rules)
    co.scan REPO, [1, 2, 3     ]
    puts co.total #=> 66.78


How to create a new rule :
========================
    
    
    # step 4 (optional) :
    #   How to define a new discounting rule?
    # Example below : never pay more than £33
    #------------------------------------------
    
    max_order_total_is_33_gbp = Rule.new.tap do |rule|
    
      def rule.apply_discount_to(order)
        max_price_in_cents = 33 * 100

        may_be_discounted = max_price_in_cents < order.subtotal_in_cents
        return unless may_be_discounted
    
        discount  = order.subtotal_in_cents - max_price_in_cents
        # apply the discount :
        order.subtotal_discount_in_cents = discount
      end
    
    end
    
    
    active_discount_rules = [ max_order_total_is_33_gbp]
    
    co = Checkout.new(active_discount_rules)
    
    co.scan REPO, [1]       ; puts co.total   #=> 9.25
    co.scan REPO, [3]       ; puts co.total   #=> 29.2
    co.scan REPO, [1, 2, 3 ]; puts co.total   #=> 33.0  <--- result of the discount
